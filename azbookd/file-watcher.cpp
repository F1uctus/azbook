#include <iostream>
#include <string>
#include <algorithm>

#include <queue>
#include <set>

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

#include <libxml++/libxml++.h>

#include "common.h"
#include "fsm.hpp"
#include "libazbook-common/config.h"
#include "file-watcher.h"
#include "file-synchronizer.h"
#include "util.h"
#include "events.h"
#include "file-utils.h"

#include "logger-client.hpp"


////

using namespace std;
using namespace xmlpp;
using namespace file_watcher;
using namespace std::placeholders;

/**
 * The main constructor of the class.
 *
 * @param config A system configuration (an instance of Config class.)
 * @param file_path A path to watch.
 */
File_watcher::File_watcher(const Config& config, string file_path)
    : Logger_client("FW"),
      FSM(INITIAL_SYNC),
      file_path(file_path),
      config(config)
{
    configure_logging();
    inotify_fd = inotify_init();
    file_utils::enable_nonblocking_mode_x(inotify_fd);
    if (config.contains_p("user_password")) {
        webdav = new WebDAV(config.get("webdav_url"),
                            config.get("user_name"),
                            config.get("user_password"),
                            config.get("ca_path"),
                            config.get("log_level") == "info");
    } else {
        webdav = new WebDAV(config.get("webdav_url"),
                            config.get("session_cookie"),
                            config.get("ca_path"),
                            config.get("log_level") == "info");
    }
    event_handler = new event_handler::Event_handler(config, webdav,
                                                     inotify_fd,
                                                     string(file_path));
    LOG_TRACE("file_path: %s", file_path.c_str());
    file_synchronizer = new File_synchronizer(file_path, webdav);
    init_fsm();
}

void File_watcher::init_fsm()
{
    add_state(INITIAL_SYNC, bind(&File_watcher::fsm_initial_sync, this, _1, _2));
    add_state(READ_EVENTS, bind(&File_watcher::fsm_read_events, this, _1, _2));
    add_state(PARSE_EVENT, bind(&File_watcher::fsm_parse_event, this, _1, _2));
    add_state(HANDLE_EVENT, bind(&File_watcher::fsm_handle_event, this, _1, _2));
    add_state(ENQUEUE_EVENT,
              bind(&File_watcher::fsm_enqueue_event, this, _1, _2));
    add_state(DEQUEUE_EVENT,
              bind(&File_watcher::fsm_dequeue_event, this, _1, _2));
}


//// FSM state handlers.

ssize_t File_watcher::read_events_x()
{
    ssize_t num_read = read(this->inotify_fd,
                            this->buf,
                            BUF_LEN);
    this->buffer_pointer = this->buf;
    return num_read;
}


fsm::result_t File_watcher::fsm_initial_sync(void* data, size_t size)
{
    sync_status = 0;
    start_time = time(nullptr);
    file_synchronizer->sync("/");
    sync_status = 50;
    file_synchronizer->push("/");
    sync_status = 75;
    event_handler->watch();
    sync_status = 100;
    return { READ_EVENTS, 0 };
}

/**
 * Read inotify events.
 *   Possible next FSM states:
 *   - PARSE_EVENT
 *   - DEQUEUE_EVENT
 *
 * @return A new File_watcher FSM state.
 */
fsm::result_t File_watcher::fsm_read_events(void* data, size_t size)
{
    fsm_state_t next_state;
    LOG_FSM_ENTER("READ_EVENTS");

    sleep(1);
    if (counter % 10)
        print_statistics();

    this->num_read = this->read_events_x();
    LOG_FSM_TRACE("READ_EVENTS", "num_read: %ld", num_read);

    if (num_read < 0) {
        if ((event_queue.size() > 0) && webdav->server_available_p()) {
            LOG_FSM_TRANSITION("READ_EVENTS", "DEQUEUE_EVENT");
            next_state = DEQUEUE_EVENT;
        } else {
            next_state = PARSE_EVENT;
        }
    } else {
        LOG_FSM_TRANSITION("READ_EVENTS", "PARSE_EVENT");
        next_state = PARSE_EVENT;
    }
    return { next_state, 0 };
}

/**
 * Parse an inotify event.
 */
fsm::result_t File_watcher::fsm_parse_event(void* data, size_t size)
{
    fsm_state_t next_state;
    LOG_FSM_ENTER("PARSE_EVENT");
    if (buffer_pointer < (buf + num_read)) {
        this->event = (struct inotify_event *) buffer_pointer;
        buffer_pointer += sizeof(struct inotify_event) + this->event->len;
        LOG_FSM_TRANSITION("PARSE_EVENT", "HANDLE_EVENT");
        ++this->events_received;
        next_state = HANDLE_EVENT;
    } else {
        LOG_FSM_TRANSITION("PARSE_EVENT", "READ_EVENTS");
        next_state = READ_EVENTS;
    }
    return { next_state, 0 };
}

fsm::result_t File_watcher::fsm_handle_event(void* data, size_t size)
{
    fsm_state_t next_state;
    LOG_FSM_ENTER("HANDLE_EVENT");
    switch (event_handler->handle_event(event)) {
    case event_handler::RESULT_EVENT_HANDLED:
        event_queue.dequeue(event);
        ++events_handled;
        LOG_FSM_TRANSITION("HANDLE_EVENT", "PARSE_EVENT");
        next_state = PARSE_EVENT;
        break;

    case event_handler::RESULT_CONNECTION_ERROR:
        LOG_FSM_WARNING("HANDLE_EVENT", "%s",
                        "Connection error.");
    case event_handler::RESULT_EVENT_POSTPONED:
        LOG_FSM_TRANSITION("HANDLE_EVENT", "ENQUEUE_EVENT");
        next_state = ENQUEUE_EVENT;
        break;
    }
    return { next_state, 0 };
}

fsm::result_t File_watcher::fsm_enqueue_event(void* data, size_t size)
{
    LOG_FSM_ENTER("ENQUEUE_EVENT");
    event_queue.enqueue(event);
    return { PARSE_EVENT, 0 };
}

fsm::result_t File_watcher::fsm_dequeue_event(void* data, size_t size)
{
    fsm_state_t next_state;
    LOG_FSM_ENTER("DEQUEUE_EVENT");
    this->event = this->event_queue.front();
    if (this->event) {
        LOG_FSM_TRACE("DEQUEUE_EVENT", "Event with EUID: %s",
                      events::make_event_uid(this->event));
        LOG_FSM_TRANSITION("DEQUEUE_EVENT", "HANDLE_EVENT");
        next_state = HANDLE_EVENT;
    } else {
        LOG_FSM_TRACE("DEQUEUE_EVENT", "queue size: %d",
                      event_queue.size());
        next_state = PARSE_EVENT;
    }
    return { next_state, 0 };
}


void File_watcher::print_statistics()
{
    LOG_INFO("%s", "---------- stat ----------");
    LOG_INFO("Events received/handled/in queue: %ld/%ld/%ld",
             this->events_received,
             this->events_handled,
             this->event_queue.size());
    LOG_INFO("Watch descriptor count:           %ld",
             this->event_handler->get_wd_count());

    time_t timediff = time(nullptr) - this->start_time;
    time_t hh = timediff / (60 * 60 * 24);
    time_t mm = (timediff % (60 * 60)) / 60;
    time_t ss = timediff % 60;
    LOG_INFO("Uptime (hh:mm:ss):                %d:%d:%d",
             hh, mm, ss);
    LOG_INFO("%s", "--------------------------");
}

int32_t File_watcher::get_sync_status() const
{
    return this->sync_status;
}

////


/**
 * Command File_watcher FSM do a single FSM step.
 */
void File_watcher::run()
{
    FSM::run(NULL, 0);
    ++counter;
}

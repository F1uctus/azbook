#ifndef __DAEMON_HPP__
#define __DAEMON_HPP__
#include <thread>
#include <dbus-c++/dbus.h>


#include "daemon-adaptor.hpp"
#include "file-watcher.h"
#include "logger-client.hpp"

extern const char* AZBOOK_DAEMON_SERVICE_NAME;
extern const char* AZBOOK_DAEMON_SERVICE_PATH;

class Daemon_exception : public std::runtime_error {
public:
    Daemon_exception(const std::string& what)
        : runtime_error(what)
        {
        }
};

class Daemon
    : public ru::gkaz::Azbook::Daemon_adaptor,
      public DBus::IntrospectableAdaptor,
      public DBus::ObjectAdaptor,
      public Logger_client
{
public:
    Daemon(DBus::Connection& connection, const Config& config);
    ~Daemon();

    int32_t GetStatus();

    void run(const std::string& watch_file);
    void stop();

private:
    file_watcher::File_watcher* file_watcher = NULL;
    thread* main_loop_thread;
    int is_running = false;

    /** Daemon configuration. */
    Config config;

    void main_loop();
};

#endif	// ifndef __DAEMON_HPP__

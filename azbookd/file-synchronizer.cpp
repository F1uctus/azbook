#include <iostream>
#include <fstream>
#include <libxml++/libxml++.h>
#include <sys/stat.h>


//// Baical
#undef TRUE
#undef FALSE
#include "P7_Trace.h"
#include "P7_Telemetry.h"


////

#include "common.h"
#include "file-utils.h"
#include "file-synchronizer.h"
#include "webdav.h"
#include "util.h"

#include "logger-client.hpp"

using namespace std;

/// Default mode for creating directories.
static const int DEFAULT_MODE = S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH;

/**
 * Constructor of the class.
 *
 * @param root_directory Root directory for synchronization.
 * @param webdav A pointer to WebDAV instance.
 */
File_synchronizer::File_synchronizer(const string& root_directory, WebDAV* webdav)
    : Logger_client("FS"),
      webdav(webdav),
      root_directory(root_directory)
{
    configure_logging();
}

void File_synchronizer::push(const string& file)
{
    string local_path;
    if (file == "/") {
        local_path = root_directory;
    } else {
        local_path = root_directory + file;
    }

    if (is_special_file_p(local_path)) {
        return;
    }

    bool is_exists = webdav->resource_exists_p(file);
    if (is_directory_p(local_path)) {
        if (! is_exists) {
            webdav->mkcol("/", file);
        }

        struct dirent* dent;
        DIR *dir = opendir(local_path.c_str());
        if (! dir) {
            LOG_WARNING("Could not open directory: %s", local_path.c_str());
            return;
        }
        while (dent = readdir(dir)) {
            if (is_current_or_parent_dir_p(dent->d_name))
                continue;

            string path;
            if (file == "/") {
                path = string("/") + dent->d_name;
            } else {
                path = file + "/" + dent->d_name;
            }
            push(path);
        }
    } else if (! is_exists) {
        webdav->upload_file(root_directory, file);
    }
}

////

/**
 * Synchronize a resource pointed by a @p node.
 *
 * @param node Resource node that have to be synchronized.
 * @param path Path to the local file (excluding the root directory.)
 * @param file Local file name.
 */
void File_synchronizer::sync_resource(const xmlpp::Node* node,
                                      const string& path, const string& file)
{
    LOG_TRACE("path: \"%s\"; file: \"%s\"; displayname: \"%s\"",
              path.c_str(), file.c_str(), prop::get_displayname(node).c_str());
    string local_path = this->root_directory + path + file;
    file_utils::smkdir(this->root_directory + path, DEFAULT_MODE);
    LOG_TRACE("local_path: \"%s\"", local_path.c_str());
    ofstream output_stream(local_path, ios::out | ios::binary);
    try {
        this->webdav->get_file(path + file, output_stream);
    } catch (Curl_exception* e) {
        LOG_WARNING("Curl error: \"%s\"", e->what());
    }
}

/**
 * Synchronize the specified a @p path and @p file using WebDAV.
 * @param path A path to the local file (excluding the root directory.)
 * @param file A local file name.
 */
void File_synchronizer::sync(const string& path, const string& file)
{
    xmlpp::Node* root_node;
    string buffer = "";
    CURLcode result = this->webdav->propfind(path, file, &buffer);
    xmlpp::DomParser parser;
    try {
        parser.parse_memory(buffer);
    } catch (xmlpp::parse_error& err) {
        LOG_CRITICAL("Could not parse XML document: %s", err.what());
        LOG_CRITICAL("Buffer:\n%s", buffer.c_str());
        throw File_synchronizer_exception("Server response: " + buffer);
    }

    root_node = parser.get_document()->get_root_node();
    LOG_TRACE("path: \"%s\"; file: \"%s\"", path.c_str(), file.c_str());

    auto list = root_node->get_children("response");
    for (const xmlpp::Node* elem : list) {
        string display_name = prop::get_displayname(elem);
        if (display_name.length() == 0)
            continue;
        if (prop::is_collection_p(elem)) {
            if ((display_name.length() > 0) && (display_name != file))
                this->sync(path + file, display_name + "/");
        } else {
            this->sync_resource(elem, path + file, display_name);
        }
    }
}

//// File_synchronizer.cpp ends here.

#include <dbus-c++/dbus.h>
#include <thread>
#include <execinfo.h>
#include <sys/stat.h>
#include <sys/types.h>

// Baical
#undef TRUE
#undef FALSE
#include "P7_Trace.h"
#include "P7_Telemetry.h"

#include "common.h"
#include "daemon.hpp"

using namespace std;

const char* AZBOOK_DAEMON_SERVICE_NAME = "ru.gkaz.Azbook.Daemon";
const char* AZBOOK_DAEMON_SERVICE_PATH = "/ru/gkaz/Azbook/Daemon";

/**
 * Daemon constructor.
 */
Daemon::Daemon(DBus::Connection& connection, const Config& config)
    : Logger_client("daemon"),
      DBus::ObjectAdaptor(connection, AZBOOK_DAEMON_SERVICE_PATH),
      config(config)
{
    configure_logging();
}

/**
 * Daemon destructor.
 */
Daemon::~Daemon()
{
    main_loop_thread->join();

    if (baical_client) {
        baical_client->Release();
    }

    delete file_watcher;
    delete baical_client;
}


/**
 * Handle DBus 'GetStatus' request.
 * @return A value in the range 0..100.
 */
int32_t Daemon::GetStatus()
{
    return file_watcher->get_sync_status();
}

void Daemon::main_loop()
{
    try {
        // The main loop.
        while (this->is_running) {
            file_watcher->run();
        }
    } catch (runtime_error* e) {
        const int ST_SIZE = 50;
        void* stack[ST_SIZE];
        size_t size = backtrace(stack, ST_SIZE);
        char** messages = backtrace_symbols(stack, size);
        cerr << "ERROR: " << e->what() << endl;
        for (int idx = 1; (idx < size) && (messages != NULL); ++idx) {
            fprintf(stderr, "[bt]: (%d) %s\n", idx, messages[idx]);
        }
    }
}

/**
 * Run the daemon.
 */
void Daemon::run(const string& watch_file)
{
    if ( (mkdir(watch_file.c_str(), S_IRWXU) != 0) && (errno != EEXIST) ) {
        throw Daemon_exception("Could not create a directory: "
                               + watch_file);
    }

    file_watcher = new file_watcher::File_watcher(config, watch_file);

    this->is_running = true;
    main_loop_thread = new thread(&Daemon::main_loop, this);
}

void Daemon::stop()
{
    this->is_running = false;
    main_loop_thread->join();
}

//// Daemon.cpp ends here.

#ifndef __FILE_WATCHER_H__
#define __FILE_WATCHER_H__

#include <sys/inotify.h>
#include <string>
#include <sys/types.h>

/* Standard data types. */
#include <queue>
#include <set>

#include "fsm.hpp"
#include "event-handler.h"
#include "event-queue.h"
#include "file-synchronizer.h"
#include "webdav.h"


////

#include "logger-client.hpp"

namespace file_watcher {

/**
 * Length of the buffer.
 */
const uint32_t BUF_LEN = (10 * (sizeof(struct inotify_event) + NAME_MAX + 1));

enum fsm_state_t {
    INITIAL_SYNC,     /**< Initial synchronisation. */
    READ_EVENTS,      /**< Read inotify events. */
    PARSE_EVENT,      /**< Parse inotify event. */
    HANDLE_EVENT,     /**< Handle inotify event. */
    ENQUEUE_EVENT,    /**< Enqueue a big file event. */
    DEQUEUE_EVENT,    /**< Dequeue a big file event. */
};

class File_watcher_exception : public runtime_error {
public:
File_watcher_exception(const std::string& what)
    : std::runtime_error(what)
    {
    }
};

class File_watcher : public Logger_client, public fsm::FSM {
public:
    File_watcher(const Config& config, std::string file_path);
    void run();
    int32_t get_sync_status() const;

private:
    /** Event queue for storing postponed inotify events. */
    Event_queue event_queue;

    /** The system configuration instance. */
    const Config& config;

    /** A pointer to a WebDAV instance. */
    WebDAV* webdav;

    /** A pointer to an Event_handler instance. */
    event_handler::Event_handler* event_handler;

    /** A pointer to an File_synchronizer instance. */
    File_synchronizer* file_synchronizer;

    std::string file_path;
    int32_t inotify_fd;
    ssize_t num_read;

    /**
     * Synchronisation status
     * (0 -- not synchronized, 100 -- fully synchronized)
     */
    int32_t sync_status = 0;

    char buf[BUF_LEN];
    char *buffer_pointer = NULL;
    struct inotify_event *event;

    uint64_t counter = 0;
    uint64_t events_received = 0;
    uint64_t events_handled = 0;
    time_t   start_time;	/** Timestamp of the daemon start. */

    void init_fsm();

    ssize_t read_events_x();

    fsm::result_t fsm_initial_sync(void* data, size_t size);
    fsm::result_t fsm_read_events(void* data, size_t size);
    fsm::result_t fsm_parse_event(void* data, size_t size);
    fsm::result_t fsm_handle_event(void* data, size_t size);
    fsm::result_t fsm_enqueue_event(void* data, size_t size);
    fsm::result_t fsm_dequeue_event(void* data, size_t size);
    fsm_state_t fsm_force_sync();

    void print_statistics();
};

}

#endif	/* ifndef __FILE_WATCHER_H__ */

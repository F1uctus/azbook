//// azbookd.cpp -- Implementation of the azbook client.

#include <iostream>
#include <fstream>
#include <filesystem>
#include <map>
#include <getopt.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <execinfo.h>


////

#include "common.h"
#include "libazbook-common/config.h"
#include "util.h"
#include "events.h"

#include "daemon.hpp"


////

using namespace std;

const bool DEBUG = true;

static struct option _long_options[] = {
    { "help",         no_argument,       0,    'h' },
    { "config",       required_argument, NULL, 'C' },
    { "dbus-address", required_argument, 0,    'B' },
    { "daemon",       no_argument,       0,    'D' },
    { "pid-file",     required_argument, NULL, 'P' },
    { 0,              0,                 0,    0   }
};

static void _write_pid_file(const string& file, pid_t pid)
{
    ofstream ofs;
    ofs.open(file);
    ofs << pid << endl;
    ofs.close();
}

static void _delete_pid_file(const string& file)
{
    remove(filesystem::path(file));
}

static bool _daemonize(const string& pid_file)
{
    // if (int fdnull = open("/dev/null", O_RDWR)) {
    //		  dup2(fdnull, STDIN_FILENO);
    //		  dup2(fdnull, STDOUT_FILENO);
    //		  dup2(fdnull, STDERR_FILENO);
    //		  close(fdnull);
    // } else {
    //		  cerr << "Filed to open '/dev/null'" << endl;
    //		  return false;
    // }
    cerr << "Forking the process..." << endl;
    int pid = fork();
    if (pid < 0)
        exit(EXIT_FAILURE);
    if (pid > 0)
        exit(EXIT_SUCCESS);

    if (setsid() < 0) {
        cerr << "Failed to detach from previous process group" << endl;
        return false;
    }
    cerr << "Forked and detached from the previous process group.  PID: " << getpid() << endl;

    cerr << "Forking..." << endl;
    pid = fork();
    if (pid < 0)
        exit(EXIT_FAILURE);
    if (pid > 0)
        exit(EXIT_SUCCESS);
    cerr << "New PID: ." << getpid() << endl;
    umask(0);
    if (chdir("/")) {
        cerr << "Could not change directory to '/'" << endl;
    }

    cerr << "Done." << endl;
    return true;
}

DBus::BusDispatcher dispatcher;
Daemon* azbook_daemon;

static IP7_Client* baical_client;
static IP7_Trace* baical_trace;

/**
 * Configure Baical logging.
 * @throws runtime_error on errors.
 */
static void _configure_logging(Config& config)
{
    string logging_configuration;
    try {
        logging_configuration = config.get("logging_configuration", false);
    } catch (out_of_range& e) {
        logging_configuration = "/P7.Sink=FileTxt /P7.Dir=/var/log/";
    }

    cerr << "Logging configuration: " << logging_configuration << endl;

    baical_client = P7_Create_Client(TM(logging_configuration.c_str()));
    if (baical_client == NULL) {
        throw new runtime_error("Could not initialize logging");
    }
    if (! baical_client->Share(P7_SHARED)) {
        cerr << "ERROR: Could not share Baical client" << endl;
    }

    baical_trace = P7_Create_Trace(baical_client, TM("azbookd"));
    baical_trace->Register_Thread(TM("azbookd"), 0);
    baical_trace->Share(P7_SHARED_TRACE);
}

/**
 * @brief _sighandler -- Signal hanlder.
 */
static void _sighandler(int signum)
{
    dispatcher.leave();
    azbook_daemon->stop();
}

static void _run(const char* config_file,
                 const string& dbus_address,
                 const string& watch_file,
                 const string& pid_file)
{
    Config config(config_file);
    bool ok = true;
    int tries = 5;
    _configure_logging(config);

    if (signal(SIGTERM, _sighandler) == SIG_ERR) {
        baical_trace->P7_ERROR(baical_trace,
                               TM("Could not register a signal handler for SIGTERM"));
        exit(1);
    }

    if (signal(SIGINT,  _sighandler) == SIG_ERR) {
        baical_trace->P7_ERROR(baical_trace,
                               TM("Could not register a signal handler for SIGINT"));
        exit(1);
    }

    baical_trace->P7_INFO(baical_trace,
                          TM("Watch file: %s"),
                          watch_file.c_str());
    baical_trace->P7_INFO(baical_trace,
                          TM("Connecting to DBus ..."));

    setenv("DBUS_SESSION_BUS_ADDRESS", dbus_address.c_str(), 0);

    do {
        try {
            DBus::default_dispatcher = &dispatcher;
            DBus::Connection conn = DBus::Connection::SessionBus();

            baical_trace->P7_INFO(baical_trace,
                                  TM("Requesting the DBus name %s ..."),
                                  AZBOOK_DAEMON_SERVICE_NAME);

            conn.request_name(AZBOOK_DAEMON_SERVICE_NAME);

            baical_trace->P7_INFO(baical_trace,
                                  TM("Creating the Daemon ..."));

            azbook_daemon = new Daemon(conn, config);

            baical_trace->P7_INFO(baical_trace,
                                  TM("Starting the Daemon ..."));

            azbook_daemon->run(watch_file);

            baical_trace->P7_INFO(baical_trace,
                                  TM("Entering the DBus dispatcher ..."));
            ok = true;
            _write_pid_file(pid_file, getpid());
            dispatcher.enter();
            baical_trace->P7_INFO(baical_trace,
                                  TM("Stopping the daemon ..."));
            azbook_daemon->stop();
            baical_trace->P7_INFO(baical_trace,
                                  TM("Stopping the daemon ... done"));
            baical_trace->P7_INFO(baical_trace,
                                  TM("Removing the PID file '%s' ..."),
                                  pid_file.c_str());
            _delete_pid_file(pid_file);
            baical_trace->P7_INFO(baical_trace,
                                  TM("Removing the PID file '%s' ... done"),
                                  pid_file.c_str());
            delete azbook_daemon;
        } catch (DBus::Error& error) {
            baical_trace->P7_CRITICAL(baical_trace,
                                      TM("Could not connect to DBus: %s"),
                                      error.what());

            ok = false;
            if (tries > 0) {
                baical_trace->P7_INFO(baical_trace,
                                      TM("Trying again in 1s ..."));

                sleep(1);
                tries--;
            } else {
                throw error;
            }
        }
    } while (! ok);
}

/**
 * Print the help message and exit.
 */
static void _print_help_and_exit(const char* pname)
{
    cout << "Usage: " << pname << "[options] <file-name>" << endl
         << "Options:" << endl
         << "  --config, -C <config-file>    Path to a config file.  [required]" << endl
         << "  --daemon, -D                  Run in the daemon mode." << endl
         << "  --dbus-address, -B <address>  Specify a DBus address to use." << endl
         << "                                If address is not specified, default" << endl
         << "                                address will be used." << endl
         << "  --pid-file, -P                Daemon lock file." << endl
         << "  --help, -h                    Print this message and exit." << endl;
    exit(0);
}


//// Entry point.

/**
 * Program entry point.
 */
int main(int argc, char** argv)
{
    int c;
    int option_index;
    char* config_file = NULL;
    bool is_daemon = false;
    char* logging = NULL;
    string pid_file = "/tmp/azbookd.pid";

    string bus_path = "unix:path=/run/user/" + to_string(getuid()) + "/bus";

    while (1) {
        c = getopt_long(argc, argv,
                        "hDC:B:P:",
                        _long_options,
                        &option_index);

        if (c == -1)
            break;

        switch (c) {
        case 0:
            if (_long_options[option_index].flag != 0)
                break;
            break;
        case 'D':
            is_daemon = true;
            break;
        case 'h':
            _print_help_and_exit(argv[0]);
            break;
        case 'C':
            config_file = optarg;
            break;
        case 'B':
            bus_path = string(optarg);
            break;
        case 'P':
            pid_file = string(optarg);
            break;
        }
    }

    if (optind >= argc)
        _print_help_and_exit(argv[0]);

    if (! config_file) {
        cerr << "Config file is not specified" << endl;
        return 1;
    }

    if (is_daemon) {
        if (_daemonize(pid_file) == false) {
            cerr << "Could not deamonize" << endl;
            return 1;
        }
        cerr << "Daemonized." << endl;
    }

    _run(config_file, bus_path, string(argv[optind]), pid_file);
    return 0;
}

//// azbookd.cpp ends here.

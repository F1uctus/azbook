#ifndef __EVENT_HANDLER_H__
#define __EVENT_HANDLER_H__

#include <map>


////

#include "webdav.h"
#include "libazbook-common/config.h"
#include "logger-client.hpp"
#include "fsm.hpp"

namespace event_handler {


/**
 * All the possible Event_handler FSM states.
 */
enum fsm_state_t {
    /**
     * Determine what kind of event we faced.
     */
    DETERMINE_EVENT,
    /**
     * Event has been handled; return the results.
     */
    EVENT_HANDLED,
    /** 
     * Make a WebDAV request to delete a file.
     */
    WEBDAV_DELETE_FILE,
    /**
     * Make a WebDAV request to delete a collection.
     */
    WEBDAV_DELETE_COLLECTION,
    /**
     * Make a WebDAV rquest to upload a changed file.
     */
    WEBDAV_UPLOAD_FILE,
    /**
     * Make a WebDAV request to make a new collection.
     */
    WEBDAV_MKDIR,
    /**
     * Check the system average load.
     */
    CHECK_LOAD_AVG,
    /**
     * Event was postponed; return the results.
     */
    EVENT_POSTPONED,
    /**
     * WebDAV connection error has been observed.
     */
    CONNECTION_ERROR,
};


/**
 * Possible outcomes of Event_handler FSM work.
 */
enum result_t {
    /**
     * Event is being handled; Event_handler FSM has not finished
     * its work yet.
     */
    RESULT_EVENT_BEING_HANDLED,
    /**
     * Event is handled; Event_handler FSM has finished its work.
     */
    RESULT_EVENT_HANDLED,
    /**
     * Event is postponed; Event_handler FSM has finished its work.
     */
    RESULT_EVENT_POSTPONED,
    /**
     * Event_handler could not contact the WebDAV server to
     * synchronize files.
     */
    RESULT_CONNECTION_ERROR,
};

class Event_handler : public Logger_client, public fsm::FSM {
public:
    Event_handler(const Config& config, WebDAV* webdav, int32_t inotify_fd,
                  const std::string& path);
    bool watch(const std::string& file);
    bool watch();
    result_t handle_event(struct inotify_event* event);
    uint64_t get_wd_count() const;
private:
    const Config& config;		/**< An instance of Config configuration. */
    WebDAV* webdav;		/**< A pointer to a WebDAV instance. */
    std::string path;
    int32_t inotify_fd;	/**< inotify file descriptor. */

    /**
     * Hash map of descriptors and corresponding files.
     */
    std::map<int, std::string> wd_map;
    uint32_t path_len;
    int file_size_max;
    double load_avg_threshold;

    void init_fsm();

    fsm::result_t fsm_determine_event(void* data, size_t size);
    fsm::result_t fsm_webdav_delete_file(void* data, size_t size);
    fsm::result_t fsm_webdav_delete_col(void* data, size_t size);
    fsm::result_t fsm_webdav_upload_file(void* data, size_t size);
    fsm::result_t fsm_webdav_mkdir(void* data, size_t size);
    fsm::result_t fsm_check_load_avg(void* data, size_t size);

    bool big_file_p(const struct inotify_event* event);
    void webdav_upload_file(const struct inotify_event* event);
    void webdav_delete_file(const struct inotify_event* event);
    void webdav_delete_collection(const struct inotify_event* event);
    void webdav_mkdir(const struct inotify_event* event);
};

}

#endif	/* ifndef __EVENT_HANDLER_H__ */

#ifndef __EVENTS_H__
#define __EVENTS_H__

#include <sys/inotify.h>

namespace events {
  bool file_modified_p(const struct inotify_event *event);
  bool file_deleted_p(const struct inotify_event *event);
  bool file_created_p(const struct inotify_event *event);
  bool dir_p(const struct inotify_event *event);
  ssize_t file_size(const std::string& path, const struct inotify_event *event);
  void print_event(struct inotify_event *event);
  struct inotify_event* copy(const struct inotify_event* event);
  std::string make_event_uid(const struct inotify_event* event);
}

#endif	/* ifndef __EVENTS_H__ */

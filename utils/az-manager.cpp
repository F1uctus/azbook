/* az-manager.cpp -- An interface to the Azbook session manager.
 *
 * Copyright (C) 2020 "AZ Company Group" LLC <https://gkaz.ru/>
 * Copyright (C) 2020 Artyom V. Poptsov <a@gkaz.ru>
 *
 * This file is part of Azbook.
 *
 * Azbook is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Azbook is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Azbook.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <dbus-c++/dbus.h>
#include <vector>
#include <string>
#include <memory>

#include "azbook-managerd/service/Manager_proxy.hpp"

class Manager_client :
    public ru::gkaz::Azbook::Manager_proxy,
    public DBus::IntrospectableProxy,
    public DBus::ObjectProxy {
public:
    Manager_client(DBus::Connection& connection, const char *path, const char *name)
        : DBus::ObjectProxy(connection, path, name) {
        // Do nothing.
    }
};

using namespace std;

DBus::BusDispatcher dispatcher;

string get_session_status(Manager_client& manager_client, string& login)
{
    return manager_client.SessionStatus(login);
}

void list_sessions(Manager_client& manager_client, bool is_verbose)
{
    vector<string> result = manager_client.ListSessions();
    for (auto& session : result) {
        cout << session;
        if (is_verbose) {
            cout << ": " << get_session_status(manager_client, session);
        }
        cout << endl;
    }
}

void print_help(const char* app_name)
{
    cerr << "Usage: " << app_name << " <action>" << endl
         << endl
         << "Actions:" << endl
         << "    list-sessions, ls        List active sessions." << endl;

}



// Entry point.
int main(int argc, char** argv)
{
    if (argc == 1) {
        print_help(argv[0]);
        exit(0);
    }

    DBus::default_dispatcher = &dispatcher;
    DBus::Connection connection = DBus::Connection::SystemBus();
    Manager_client manager_client(connection,
                                  "/ru/gkaz/Azbook/Manager",
                                  "ru.gkaz.Azbook.Manager");

    string action(argv[1]);

    if ((action == "list-sessions") || (action == "ls")) {
        bool is_verbose = false;
        if (argc == 3) {
            string arg(argv[2]);
            is_verbose = (arg == "-v");
        }

        list_sessions(manager_client, is_verbose);
    }

    return 0;
}

//// az-manager.cpp ends here.

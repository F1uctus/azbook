#include <iostream>
#include <getopt.h>

#include "libazbook-common/config.h"
#include "webdav.h"

using namespace std;

static struct option _long_options[] = {
     { "help",   no_argument,       0, 'h' },
     { "config", required_argument, 0, 'c' },
     { 0,        0,                 0, 0   }
};

static void _print_help_and_exit(char* pname)
{
     cout << "Usage: " << pname << " [options] <webdav-path>" << endl;
     exit(0);
}

int main(int argc, char** argv)
{
     int option_index;
     int c;
     string config_path = "/etc/azbook";
     while (1) {
	  c = getopt_long(argc, argv,
			  "hc:",
			  _long_options,
			  &option_index);
	  if (c == -1)
	      break;

	  switch (c) {
	  case 0:
	       if (_long_options[option_index].flag != 0)
		    break;
	       break;
	  case 'h':
	       _print_help_and_exit(argv[0]);
	       break;
	  case 'c':
	       cout << optarg << endl;
	       config_path = string(optarg);
	       break;
	  }
     }

     if (optind == argc) {
	  cerr << "ERROR: WebDAV path not specified" << endl;
	  return 1;
     }
     
     Config config(config_path);
     WebDAV webdav(config.get("webdav_url"),
                   config.get("user_name"),
                   config.get("user_password"),
                   config.get("ca_path"),
                   true);
     string response = "";
     int result = webdav.propfind("/",
				  string(argv[optind]),
				  &response);
     cout << response << endl;
     return 0;
}

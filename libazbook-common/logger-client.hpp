#ifndef __LOGGER_CLIENT_H__
#define __LOGGER_CLIENT_H__

#include <stdexcept>
#include <string>

// Baical
#undef FALSE
#undef TRUE

#include "P7_Trace.h"
#include "P7_Telemetry.h"

#define LOG(level, fmt, ...)                          \
    do {                                                 \
        baical_trace->P7_ ## level (baical_trace_module, \
                                    TM(fmt),             \
                                    __VA_ARGS__);        \
    } while (0)

#define LOG_DEBUG(fmt, ...)                      \
    LOG(DEBUG, fmt, __VA_ARGS__)

#define LOG_TRACE(fmt, ...)                      \
    LOG(TRACE, fmt, __VA_ARGS__)

#define LOG_INFO(fmt, ...)                     \
    LOG(INFO, fmt, __VA_ARGS__)

#define LOG_CRITICAL(fmt, ...)                   \
    LOG(CRITICAL, fmt, __VA_ARGS__)

#define LOG_WARNING(fmt, ...)                  \
    LOG(WARNING, fmt, __VA_ARGS__)


#define LOG_FSM(level, fmt, ...)                              \
    do {                                                      \
        baical_trace->P7_ ## level (baical_fsm_trace_module,  \
                                    TM(fmt),                  \
                                    __VA_ARGS__);             \
    } while (0)

#define LOG_FSM_ENTER(state) \
    LOG_FSM(TRACE, "[%s]", state)

#define LOG_FSM_TRANSITION(from, to)             \
    LOG_FSM(TRACE, "[%s] -> [%s]", from, to)

#define LOG_FSM_WARNING(current_state, fmt, ...) \
    LOG_FSM(WARNING, "[" current_state "] " fmt, __VA_ARGS__)

#define LOG_FSM_TRACE(current_state, fmt, ...)                  \
    LOG_FSM(TRACE, "[" current_state "] " fmt, __VA_ARGS__)

#define LOG_FSM_DEBUG(current_state, fmt, ...)              \
    LOG_FSM(DEBUG, "[" current_state "] " fmt, __VA_ARGS__)



class Logger_client_exception : public std::runtime_error {
public:
Logger_client_exception(const std::string& what)
    : runtime_error(what)
    {
    }
};

class Logger_client {
protected:
    Logger_client(const std::string& module_name);
    void configure_logging();
    void register_modules();

    IP7_Client* baical_client  = nullptr;
    IP7_Trace*  baical_trace   = nullptr;
    IP7_Trace::hModule baical_trace_module;
    IP7_Trace::hModule baical_fsm_trace_module;

private:
    const std::string module_name;
    const std::string fsm_module_name;
};

#endif  // ifndef __LOGGER_CLIENT_H__

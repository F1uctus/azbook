#include "config.h"
#include <iostream>
using namespace std;

Config::Config(string file_name)
     : DSV(file_name)
{
     
}

Config::Config(const Config& config)
     : DSV(config)
{
     // Do nothing.
}

enum {
     READ,
     CHECK_VAR,
     PARSE_NAME,
     SUBSTITUTE
};

/**
 * @throws Config_exception
 */
const string Config::get(const string key, bool substitute_vars) const
{
     string value;

     try {
          value = DSV::get(key);
     } catch (std::out_of_range& e) {
          throw Config_exception("Could not find key: " + key);
     }

     if (! substitute_vars) {
          return value;
     }

     string result = "";
     string option_name = "";
     int state = READ;
     for (int idx = 0; idx < value.length(); ++idx) {
          switch (state) {
          case READ:
               if (value[idx] == '$') {
                    state = CHECK_VAR;
               } else {
                    result += value[idx];
               }
               break;

          case CHECK_VAR:
               if (value[idx] == '{') {
                    option_name = "";
                    state = PARSE_NAME;
               } else {
                    state = READ;
               }
               break;
          case PARSE_NAME:
               if (value[idx] == '}') {
                    string option;
                    try {
                         option = DSV::get(option_name);
                    } catch (std::out_of_range& e) {
                         throw Config_exception("Could not find substitute: " + option_name);
                    }
                    result += option;
                    state = READ;
               } else {
                    option_name += value[idx];
               }
               break;
          }
     }
     return result;
}

/* dsv.cpp -- Ad-Hoc implementation of DSV parser/writer.
 *
 * Copyright (C) 2018 Artyom V. Poptsov <poptsov.artyom@gmail.com>
 *
 * This file is part of MST.
 *
 * MST is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * MST is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MST.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include <vector>

#include "DSV.hpp"
#include "util.h"

using namespace std;

/**
 * Split an @p input string using a @p separator.
 *
 * @param input An input string.
 * @param separator A separator to use.
 * @return A vetor of strings.
 */
static vector<string> _split(string input, char separator) {
    int p = input.find(separator);
    vector<string> result;
    result.push_back(input.substr(0, p));
    result.push_back(input.substr(p + 1));
    return result;
}


static void _parse_x(ifstream& in, map<string, string>& out)
{
    for (string line = ""; getline(in, line); )
    {
        vector<string> data = _split(line, ':');
        out[data[0]] = data[1];
    }
}

/**
 * The main constructor of the class.
 *
 * @param file_name Name of a file with configuration.
 * @throw DSV_exception when file could not be opened.
 */
DSV::DSV(string file_name)
    : file_name(file_name)
{
    ifstream in(file_name);
    if (! in.is_open())
    {
	 DSV_exception exception("Could not open config file: " + file_name);
	 throw exception;
    }

    _parse_x(in, data);
    in.close();
}

DSV::DSV(const DSV& dsv)
     : file_name(dsv.file_name),
       data(dsv.data)
{
     // Do nothing.
}

/**
 * @brief DSV::get -- get a parameter value from a config.
 * @param name -- a parameter name to search for.
 * @return the parameter value.
 * @throws std::out_of_range when no option is found.
 */
const string DSV::get(const string name) const
{
    return data.at(name);
}

/**
 * @brief DSV::contains_p -- Checks if the data contains @a key.
 * @param key -- a parameter name to search for.
 * @return true if the key is found, false otherwise.
 */
bool DSV::contains_p(const std::string key) const
{
    return data.find(key) != data.end();
}

/**
 * @brief DSV::remove -- remove a key.
 * @param name -- Name of a key.
 */
void DSV::remove(const string& name)
{
     data.erase(name);
}

/**
 * @brief DSV::put -- put a new parameter value to a config.
 * @param name -- name of the parameter.
 * @param value -- value of the parameter.
 */
void DSV::put(const string& name, const string& value)
{
    data[name] = value;
}

const string DSV::get_file_name() const
{
     return this->file_name;
}

void DSV::set_file_name(const string& file_name)
{
     this->file_name = file_name;
}

/**
 * @brief DSV::save -- save DSV data to a file.
 *
 * XXX: This method does not escape special characters.
 */
void DSV::save()
{
    ofstream out(file_name, ios::trunc);
    for (auto const& record : data)
    {
        out << record.first << ":" << record.second << endl;
    }
}

//// WebDAV.cpp -- Implementation of WebDAV methods.

#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <iostream>
#include <string>

#include <curl/curl.h>

#include <libxml++/libxml++.h>

#include "common.h"
#include "logger-client.hpp"
#include "webdav.h"


////

using namespace std;


//// Helper procedures.

/**
 * Initialize CURL instsance.
 *
 * @return A pointer to a new CURL instance.
 */
static CURL* _init_curl()
{
     CURL* curl = curl_easy_init();
     if (! curl) {
          fprintf(stderr, "[ERROR] could not init CURL\n");
          throw new Curl_exception("Could not create a CURL session.");
     }

     return curl;
}

/* ioctl callback function */
static curlioerr my_ioctl(CURL *handle, curliocmd cmd, void *userp)
{
     int *fdp = (int *)userp;
     int fd = *fdp;

     (void)handle; /* not used in here */

     switch(cmd) {
     case CURLIOCMD_RESTARTREAD:
          /* mr libcurl kindly asks as to rewind the read data stream to start */
          if(-1 == lseek(fd, 0, SEEK_SET))
               /* couldn't rewind */
               return CURLIOE_FAILRESTART;

          break;

     default: /* ignore unknown commands */
          return CURLIOE_UNKNOWNCMD;
     }
     return CURLIOE_OK; /* success! */
}

/* read callback function, fread() look alike */
static size_t read_callback(void *ptr, size_t size, size_t nmemb, void *stream)
{
     WebDAV_file* file_pointer = (WebDAV_file *) stream;
     WebDAV_file  file = *file_pointer;
     curl_off_t nread = read(file.fd, ptr, (size_t) (size * nmemb));
     off_t offset = lseek(file.fd, 0, SEEK_CUR);

     if (! file.webdav->is_quiet_p()) {
         fprintf(stderr, "*** We read %" CURL_FORMAT_CURL_OFF_T
                 " bytes (%ld/%ld) from %s\n",
                 (curl_off_t) nread,
                 offset,
                 file.size,
                 file.name.c_str());
     }

     if (nread == 0) {
         fprintf(stderr, "Uploaded file '%s' (%ld bytes)\n",
                 file.name.c_str(), file.size);
         close(file.fd);
     }

     if (nread == -1) {
         fprintf(stderr, "ERROR: Failed to read file '%s'\n",
                 file.name.c_str());
         close(file.fd);
         return 0;
     }

     return nread;
}

static void _handle_curl_error(CURLcode result)
{
     fprintf(stderr, "[ERROR] could not make a CURL request: %s\n",
             curl_easy_strerror(result));
}


//// WebDAV file.
WebDAV_file::WebDAV_file(WebDAV* webdav, int fd, string name,
                         string url)
     : webdav(webdav), fd(fd), stream(nullptr), size(size), name(name), url(url)
{

    struct stat file_info;
    fstat(fd, &file_info);
    size = file_info.st_size;
}

WebDAV_file::WebDAV_file(WebDAV* webdav, ofstream* stream, string name,
                         string url)
    : webdav(webdav), fd(-1), stream(stream), size(0), name(name), url(url)
{

}


//// Mehods.

/**
 * The main class constructor.
 *
 * @param server_addr The address of a WebDAV server.
 * @param user_name User name to login with.
 * @param user_password Password of the user.
 */
WebDAV::WebDAV(string server_addr, string user_name, string user_password,
               const string& ca_path, bool is_quiet)
    : Logger_client("WebDAV"),
      server_addr(server_addr),
      user_name(user_name),
      user_password(user_password),
      ca_path(ca_path),
      is_quiet(is_quiet)
{
     configure_logging();
     baical_trace->P7_INFO(baical_trace_module,
                           TM("server: \"%s\""),
                           server_addr.c_str());
     baical_trace->P7_INFO(baical_trace_module,
                           TM("user: \"%s\""),
                           user_name.c_str());
     baical_trace->P7_INFO(baical_trace_module,
                           TM("ca_path: \"%s\""),
                           ca_path.c_str());
     baical_trace->P7_INFO(baical_trace_module,
                           TM("cookie file: \"%s\""),
                           session_cookie_file.c_str());
}

WebDAV::WebDAV(string server_addr, string session_cookie_file,
               const string& ca_path, bool is_quiet = true)
    : Logger_client("WebDAV"),
      server_addr(server_addr),
      session_cookie_file(session_cookie_file),
      ca_path(ca_path),
      is_quiet(is_quiet)
{
    configure_logging();
    baical_trace->P7_INFO(baical_trace_module,
                          TM("server: \"%s\""),
                          server_addr.c_str());
    baical_trace->P7_INFO(baical_trace_module,
                          TM("user: \"%s\""),
                          user_name.c_str());
    baical_trace->P7_INFO(baical_trace_module,
                          TM("ca_path: \"%s\""),
                          ca_path.c_str());
    baical_trace->P7_INFO(baical_trace_module,
                          TM("cookie file: \"%s\""),
                          session_cookie_file.c_str());
}

bool WebDAV::is_quiet_p() const
{
    return is_quiet;
}

/**
 * Set authentication methods for a @p curl intance.
 *
 * @param curl A CURL instance.
 */
void WebDAV::curl_setauth(CURL* curl)
{
     if (! user_password.empty()) {
          string userpass = user_name + ":" + user_password;
          curl_easy_setopt(curl, CURLOPT_HTTPAUTH, (long) CURLAUTH_ANY);
          curl_easy_setopt(curl, CURLOPT_USERPWD, userpass.c_str());
     } else {
          curl_easy_setopt(curl, CURLOPT_COOKIEFILE, this->session_cookie_file.c_str());
     }
}

void WebDAV::curl_set_ssl(CURL* curl)
{
     if (this->ca_path.length() > 0)
          curl_easy_setopt(curl, CURLOPT_CAINFO, this->ca_path.c_str());
}

void WebDAV::curl_configure(CURL* curl)
{
     curl_setauth(curl);
     curl_set_ssl(curl);
     if (is_quiet) {
          curl_easy_setopt(curl, CURLOPT_STDERR, fopen("/dev/null", "w"));
          curl_easy_setopt(curl, CURLOPT_VERBOSE, 0);
     } else {
          curl_easy_setopt(curl, CURLOPT_VERBOSE, 1);
     }
}

/**
 * Convert a @p file_name to a full WebDAV path.
 *
 * @param file_name A local file name.
 * @return A full path to a file on the current WebDAV server.
 */
string WebDAV::file_name_to_webdav_path(const string& file_name)
{
     return server_addr + file_name;
}

/**
 * Upload a file to a WebDAV server.
 *
 * @param path -- path to the file
 * @param file_name -- name of the file
 * @throws Curl_connection_exception when a WebDAV server is unavailable.
 * @throws Curl_exception on generic CURL errors.
 * @return CURL code.
 */
CURLcode WebDAV::upload_file(const string& path, const string& file_name)
{
     CURL* curl = _init_curl();
     baical_trace->P7_TRACE(baical_trace_module,
                            TM("path: \"%s\"; file_name: \"%s\""),
                            path.c_str(),
                            file_name.c_str());
     string url = file_name_to_webdav_path(file_name);
     string full_name = path + file_name;
     CURLcode result;
     int hd;
     baical_trace->P7_DEBUG(baical_trace_module, TM("full_name: \"%s\""),
                            full_name.c_str());
     baical_trace->P7_DEBUG(baical_trace_module, TM("URL: \"%s\""),
                            url.c_str());

     curl_configure(curl);
     curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);

     hd = open(full_name.c_str(), O_RDONLY);

     WebDAV_file file(this, hd, full_name, url);

     curl_easy_setopt(curl, CURLOPT_READDATA, (void *) &file);
     /* and give the size of the upload, this supports large file sizes
        on systems that have general support for it */
     curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE, file.size);

     /* we want to use our own read function */
     curl_easy_setopt(curl, CURLOPT_READFUNCTION, read_callback);
     /* set the ioctl function */
     curl_easy_setopt(curl, CURLOPT_IOCTLFUNCTION, my_ioctl);
     /* pass the file descriptor to the ioctl callback as well */
     curl_easy_setopt(curl, CURLOPT_IOCTLDATA, (void *) &hd);

     curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
     result = curl_easy_perform(curl);
     close(hd);
     baical_trace->P7_DEBUG(baical_trace_module,
                            TM("result: \"%d\""),
                            result);
     switch (result) {
     case CURLE_OK:
          curl_easy_cleanup(curl);
          return result;

     case CURLE_COULDNT_CONNECT:
          throw new Curl_connection_exception(result);

     default:
          throw new Curl_exception(result);
     }
}

static size_t _file_write_callback(void *ptr, size_t size, size_t nmemb,
                                   WebDAV_file& file)
{
     ofstream* stream = file.stream;
     long before = stream->tellp();
     stream->write((const char*) ptr, size * nmemb);
     long written = stream->tellp() - before;
     if (! file.webdav->is_quiet_p()) {
          fprintf(stderr, "*** We wrote %" CURL_FORMAT_CURL_OFF_T
                  " bytes to file\n", written);
     }

     if (written < (size * nmemb)) {
         fprintf(stderr, "File downloaded: %s (%" CURL_FORMAT_CURL_OFF_T
                 " bytes)\n", file.name.c_str(), before);
     }

     return written;
}

/**
 * Download a file.
 *
 * @param webdav_path The path to a WebDAV resource.
 * @param output_stream An output stream to write the resource to.
 * @throws Curl_connection_exception when a WebDAV server is unavailable.
 * @throws Curl_exception on generic CURL errors.
 */
CURLcode WebDAV::get_file(const string& webdav_path,
                          ofstream& output_stream)
{
     CURL* curl = _init_curl();
     CURLcode result;
     string url = file_name_to_webdav_path(webdav_path);

     baical_trace->P7_TRACE(baical_trace_module,
                            TM("webdav_path: %s"),
                            webdav_path.c_str());

     WebDAV_file file(this, &output_stream,
                      webdav_path, webdav_path);

     curl_configure(curl);

     curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
     curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, _file_write_callback);
     curl_easy_setopt(curl, CURLOPT_WRITEDATA, &file);
     result = curl_easy_perform(curl);
     baical_trace->P7_DEBUG(baical_trace_module,
                            TM("result: \"%d\""),
                            result);
     switch (result) {
     case CURLE_OK:
          curl_easy_cleanup(curl);
          return result;

     case CURLE_COULDNT_CONNECT:
          baical_trace->P7_WARNING(baical_trace_module,
                                   TM("Could not connect: \"%d\""),
                                   result);
          curl_easy_cleanup(curl);
          throw new Curl_connection_exception(result);

     default:
          baical_trace->P7_WARNING(baical_trace_module,
                                   TM("Generic error: \"%d\""),
                                   result);
          curl_easy_cleanup(curl);
          throw new Curl_exception(result);
     }
}

/**
 * Delete a WebDAV resource.
 *
 * @param path The path to a resource.
 * @param file_name Name of a resource.
 * @throws Curl_connection_exception when a WebDAV server is unavailable.
 * @throws Curl_exception on generic CURL errors.
 * @return CURL code.
 */
CURLcode WebDAV::delete_file(const string& path, const string& file_name)
{
     CURL* curl = _init_curl();
     string url = file_name_to_webdav_path(file_name);
     CURLcode result;

     curl_configure(curl);

     baical_trace->P7_DEBUG(baical_trace_module, TM("URL: \"%s\""),
                            url.c_str());

     curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
     curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "DELETE");
     curl_easy_setopt(curl, CURLOPT_POST, 0L);

     result = curl_easy_perform(curl);
     baical_trace->P7_DEBUG(baical_trace_module,
                            TM("result: \"%d\""),
                            result);
     switch (result) {
     case CURLE_OK:
          curl_easy_cleanup(curl);
          return result;

     case CURLE_COULDNT_CONNECT:
          throw new Curl_connection_exception(result);

     default:
          throw new Curl_exception(result);
     }
}

/**
 * @throws Curl_exception on errors.
 */
CURLcode WebDAV::mkcol(const string& path, const string& file_name)
{
     CURL* curl = _init_curl();
     string url = file_name_to_webdav_path(file_name) + "/";
     CURLcode result;

     curl_configure(curl);

     baical_trace->P7_DEBUG(baical_trace_module, TM("URL: \"%s\""),
                            url.c_str());

     curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
     curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "MKCOL");
     curl_easy_setopt(curl, CURLOPT_POST, 0L);
     result = curl_easy_perform(curl);
     baical_trace->P7_DEBUG(baical_trace_module,
                            TM("result: \"%d\""),
                            result);
     switch (result) {
     case CURLE_OK:
          curl_easy_cleanup(curl);
          return result;

     case CURLE_COULDNT_CONNECT:
          throw new Curl_connection_exception(result);

     default:
          throw new Curl_exception(result);
     }
}

static int _write_callback(char* contents, size_t sz, size_t nmemb, void* userp)
{
     size_t length = sz * nmemb;
     ((string*) userp)->append(contents, length);
     return length;
}

/**
 * Get properties of a WebDAV resource.
 *
 * @param[in]  path The path to a resource.
 * @param[in]  file_name Name of a resource.
 * @param[out] response A pointer to a string to save the server response.
 * @throws Curl_connection_exception when a WebDAV server is unavailable.
 * @throws Curl_exception on generic CURL errors.
 * @return CURL code.
 */
CURLcode WebDAV::propfind(const string& path, const string& file_name,
                          string* response)
{
     CURL* curl = _init_curl();
     string url = file_name_to_webdav_path(path + "/" + file_name);
     CURLcode result;
     long response_code = 0;

     baical_trace->P7_TRACE(baical_trace_module,
                            TM("path: \"%s\"; file_name: \"%s\""),
                            path.c_str(),
                            file_name.c_str());

     curl_configure(curl);

     curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
     curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PROPFIND");
     curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, _write_callback);
     curl_easy_setopt(curl, CURLOPT_WRITEDATA, response);

     cout << *response << endl;

     result = curl_easy_perform(curl);
     baical_trace->P7_DEBUG(baical_trace_module,
                            TM("result: \"%d\""),
                            result);
     switch (result) {
     case CURLE_OK:
          curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
          curl_easy_cleanup(curl);
          return (CURLcode) response_code;

     case CURLE_COULDNT_CONNECT:
          throw new Curl_connection_exception(result);

     default:
          throw new Curl_exception(result);
     }
}

CURLcode WebDAV::propfind(const string& path, const string& file_name,
                          xmlpp::Node** response)
{
     string buffer = "";
     CURLcode result = propfind(path, file_name, &buffer);
     xmlpp::DomParser parser;
     parser.parse_memory(buffer);
     *response
          = parser.get_document()->get_root_node();
     return result;
}

/**
 * WebDAV::authenticate -- Try to authenicate with a WebDAV server.

 * @param cookie_jar Name of a file to write cookies to. The file will be used
 *     by the Azbook daemon to access the WebDAV interface.
 * @return CURL code.
 */
CURLcode WebDAV::authenticate(string& cookie_jar)
{
     CURL* curl = _init_curl();
     string url = file_name_to_webdav_path("/");
     CURLcode result;
     long response_code = 0;
     curl_configure(curl);

     curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
     curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PROPFIND");
     // curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, _write_callback);
     curl_easy_setopt(curl, CURLOPT_COOKIEJAR, cookie_jar.c_str());

     result = curl_easy_perform(curl);
     baical_trace->P7_DEBUG(baical_trace_module,
                            TM("result: \"%d\""),
                            result);
     switch (result) {
     case CURLE_OK:
          curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
          curl_easy_cleanup(curl);
          return (CURLcode) response_code;

     case CURLE_COULDNT_CONNECT:
          throw new Curl_connection_exception(result);

     default:
          throw new Curl_exception(result);
     }
}

bool WebDAV::resource_exists_p(const string& resource)
{
    CURL* curl = _init_curl();
    string url = file_name_to_webdav_path(resource);
    CURLcode result;
    long response_code = 0;

    baical_trace->P7_TRACE(baical_trace_module,
                           TM("resource: \"%s\""),
                           resource.c_str());

    curl_configure(curl);

    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "PROPFIND");

    result = curl_easy_perform(curl);
    baical_trace->P7_DEBUG(baical_trace_module,
                           TM("result: \"%d\""),
                           result);

    switch (result) {
    case CURLE_OK:
        curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &response_code);
        return (response_code >= 200) && (response_code < 300);
    default:
        return false;
    }
}

bool WebDAV::server_available_p()
{
     CURL* curl = _init_curl();
     string url = file_name_to_webdav_path("/");
     CURLcode result;
     curl_configure(curl);
     curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
     curl_easy_setopt(curl, CURLOPT_HEADER, 1); 
     curl_easy_setopt(curl, CURLOPT_NOBODY, 1);
     result = curl_easy_perform(curl);
     curl_easy_cleanup(curl);
     return (result == CURLE_OK);
}


//// PROPFIND procedures.

// Namespace map for WebDAV.
xmlpp::Node::PrefixNsMap prop::nsmap = { { "D", "DAV:" },
                                         { "d", "DAV:" } };

/**
 * Get content of a NODE.
 */
static const xmlpp::ContentNode* _get_content_node(const xmlpp::Node* node)
{
     const xmlpp::Node* content_node = node->get_children().front();
     return dynamic_cast<const xmlpp::ContentNode*>(content_node);
}

/**
 * Find the first child node of a NODE using an XPATH.
 */
const xmlpp::Node* _find_first(const xmlpp::Node* node,
                               const string& xpath)
{
     const_NodeSet node_set = node->find(xpath, prop::nsmap);
     if (node_set.size() == 0)
          return NULL;
     return node_set[0];
}


/**
 * Check if a NODE is a collection.
 * @return 'true' if node is a collection, 'false' otherwise.
 */
bool prop::is_collection_p(const xmlpp::Node* node)
{
     const xmlpp::Node* resource_type
          = _find_first(node, "./D:propstat/D:prop/D:resourcetype/D:collection");
     return resource_type != NULL;
}

/**
 * Get the last modification time of a NODE as a string.
 * @param node -- an XML node to parse.
 * @return modification time as a string.
 */
string prop::get_mtime_string(const xmlpp::Node* node)
{
     const xmlpp::Node* getlastmodified
          = _find_first(node, "./D:propstat/D:prop/D:getlastmodified");
     const auto content_node = _get_content_node(getlastmodified);
     if (content_node) {
          return content_node->get_content();
     }
     return "";
}



/**
 * Get display name of a resource described by a NODE.
 * @param node -- an XML node to parse.
 * @return a string value of the display name.
 */
string prop::get_displayname(const xmlpp::Node* node)
{
     const xmlpp::Node* displayname = _find_first(node, "./D:href");
     if (! displayname)
          throw new WebDAV_exception("Could not locate 'href' node");
     const xmlpp::Node* root_path = _find_first(node, "/D:multistatus/D:response/D:href");
     if (! root_path)
          throw new WebDAV_exception("Could not find the root node path");
     const auto content_node = _get_content_node(displayname);

     if (content_node) {
          string content = content_node->get_content();
          string root_path_str = _get_content_node(root_path)->get_content();
          if (root_path_str == content)
               return "";
          size_t slash_index = content.rfind("/");
          size_t str_length = content.length();
          if (slash_index == (str_length - 1)) {
               size_t name_begin = content.rfind("/", str_length - 2);
               return content.substr(name_begin + 1,
                                     str_length - name_begin - 2);
          } else {
               return content.substr(slash_index + 1, str_length - slash_index);
          }
     }
     return "";
}

// Default format of 'getlastmodified' property of an WebDAV resource.
static const char WEBDAV_TIME_FMT[] = "%a, %d %b %Y %T %Z";

/**
 * Get a NODE last modification time.
 */
time_t prop::get_mtime(const xmlpp::Node* node)
{
     string ts_string = get_mtime_string(node);
     struct tm ts_tm;
     if (! strptime(ts_string.c_str(), WEBDAV_TIME_FMT, &ts_tm)) {
          string msg = "Unable to convert date and time to a timestamp";
          // baical_trace->P7_ERROR(baical_trace_module, TM("%s: \"%s\""),
          // 			 msg.c_str(),
          // 			 ts_string.c_str());
          throw msg;
     }

     return mktime(&ts_tm);
}

//// WebDAV.cpp ends here.


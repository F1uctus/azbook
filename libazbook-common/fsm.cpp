/* fsm.hpp -- Finite-state machine implementation.
 *
 * Copyright (C) 2020 "AZ Company Group" LLC <https://gkaz.ru/>
 * Copyright (C) 2020 Artyom V. Poptsov <a@gkaz.ru>
 *
 * This file is part of Azbook.
 *
 * Azbook is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Azbook is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Azbook.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fsm.hpp"

using namespace fsm;

FSM::FSM(state_t initial_state)
{
    m_state = initial_state;
}

void FSM::add_state(state_t state, callback_t callback)
{
    m_transition_table[state] = callback;
}

int FSM::run(void* data, size_t size)
{
    result_t result = m_transition_table[m_state](data, size);
    m_state = result.next_state;
    return result.result;
}

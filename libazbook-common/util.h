#ifndef __UTIL_H__
#define __UTIL_H__

#include <sys/stat.h>
#include <string>

using namespace std;

bool is_directory_p(const string& path);
bool is_special_file_p(const string& path);
bool is_current_or_parent_dir_p(const char* d_name);
size_t get_file_size(const char* file_name);
double get_load_avg();
int compare_mtimes(const string& ts1, const string& ts2);
int mkpath(const string& path, mode_t mode);

#endif	/* ifndef __UTIL_H__ */

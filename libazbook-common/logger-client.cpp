#include <string>

#include "common.h"
#include "logger-client.hpp"

using namespace std;


Logger_client::Logger_client(const std::string& module_name)
    : module_name(module_name),
      fsm_module_name(string(module_name + "/FSM"))
{
    // Do nothing.
}

void Logger_client::register_modules()
{
    baical_trace->Register_Module(TM(module_name.c_str()),
                                  &baical_trace_module);
    baical_trace->Register_Module(TM(fsm_module_name.c_str()),
                                  &baical_fsm_trace_module);
}


/**
 * Configure Baical logging.
 * @throws runtime_error
 */
void Logger_client::configure_logging()
{
    baical_client = P7_Get_Shared(P7_SHARED);
    if (! baical_client) {
        throw new runtime_error("Could not get Baical client.");
    }

    baical_trace  = P7_Get_Shared_Trace(P7_SHARED_TRACE);
    if (! baical_trace) {
        throw new runtime_error("Could not get Baical shared trace.");
    }

    register_modules();
}

#ifndef __WEBDAV_H__
#define __WEBDAV_H__

#include <curl/curl.h>
#include <string>
#include <stdexcept>
#include <fstream>
#include <vector>

#include <libxml++/libxml++.h>

#include "../config.h"


////

#include "logger-client.hpp"

#ifdef HAVE_LIBXML_CPP_2
using const_NodeSet = std::vector<xmlpp::Node*>;
#else
using const_NodeSet = xmlpp::Node::const_NodeSet;
#endif

class WebDAV_exception : public std::runtime_error {
public:
WebDAV_exception(const std::string& what)
     : runtime_error(what)
     {
     }
};

class Curl_exception : public std::runtime_error {
public:
Curl_exception(CURLcode code)
     : runtime_error(curl_easy_strerror(code)),
          code(code) {
               /* Do nothing. */
          }
Curl_exception(const std::string& what)
     : runtime_error(what),
          code(-1) {
          /* Do nothing. */
     }
     int get_code() const {
          return this->code;
     }
private:
     int code;
};

class Curl_connection_exception : public Curl_exception {
public:
Curl_connection_exception(CURLcode code)
     : Curl_exception(code) {
          /* Do nothing. */
     }
};


class WebDAV : public Logger_client {
public:
     WebDAV(std::string server_addr,
            std::string user_name,
            std::string user_password,
            const std::string& ca_path,
            bool is_quiet);
     WebDAV(std::string server_addr,
            std::string session_cookie_file,
            const std::string& ca_path,
            bool is_quiet);
     CURLcode authenticate(std::string& cookie_jar);
     CURLcode upload_file(const std::string& path,
                          const std::string& file_name);
     CURLcode get_file(const std::string& webdav_path,
                       std::ofstream& output_stream);
     CURLcode delete_file(const std::string& path,
                          const std::string& file_name);
     CURLcode mkcol(const std::string& path,
                    const std::string& file_name);
     CURLcode propfind(const std::string& path,
                       const std::string& file_name,
                       std::string* response);
     CURLcode propfind(const std::string& path,
                       const std::string& file_name,
                       xmlpp::Node** response);
     bool resource_exists_p(const std::string& resource);
     bool server_available_p();
     bool is_quiet_p() const;

private:
     /** Address of a WebDAV server. */
     std::string server_addr;
     /** WebDAV user name. */
     std::string user_name;
     /** WebDAV user password. */
     std::string user_password;
     /** A pointer to a CURL instance. */
     CURL *curl;
     /** Path to CA certificates. */
     std::string ca_path;

     /** Session cookie file for authentication. Azbook favors cookie
         authentication over the basic auth so when the cookie file is set
         'user_name' and 'user_password' parameters are ignored. */
     std::string session_cookie_file;

     bool is_quiet;

     std::string file_name_to_webdav_path(const std::string& file_name);
     void curl_setauth(CURL* curl);
     void curl_set_ssl(CURL* curl);
     void curl_configure(CURL* curl);
};

class WebDAV_file {
public:
    WebDAV_file(WebDAV* webdav, int fd, std::string name, std::string url);
    WebDAV_file(WebDAV* webdav,
                std::ofstream* stream,
                std::string name,
                std::string url);
    WebDAV* webdav;
    int fd;
    std::ofstream* stream;
    curl_off_t size;
    std::string name;
    std::string url;
};

namespace prop {
     extern xmlpp::Node::PrefixNsMap nsmap;
     bool is_collection_p(const xmlpp::Node* node);
     time_t get_mtime(const xmlpp::Node* node);
     std::string get_mtime_string(const xmlpp::Node* node);
     std::string get_displayname(const xmlpp::Node* node);
}

#endif  /* ifndef __WEBDAV_H__ */

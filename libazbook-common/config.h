#ifndef __CONFIG_H__
#define __CONFIG_H__

#include "DSV.hpp"

class Config_exception : public std::runtime_error {
 public:
  Config_exception(const std::string& what)
       : std::runtime_error(what)
  {
  }
};

class Config : public DSV {
public:
     Config(std::string file_name);
     Config(const Config& config);
     const std::string get(const std::string key, bool substitute_vars = true) const;
};

#endif	/* ifndef __CONFIG_H__ */

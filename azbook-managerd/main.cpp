/* main.cpp -- Azbook manager main code.
 *
 * Copyright (C) 2018-2020 "AZ Company Group" LLC <https://gkaz.ru/>
 * Copyright (C) 2018-2020 Artyom V. Poptsov <a@gkaz.ru>
 *
 * This file is part of Azbook.
 *
 * Azbook is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Azbook is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Azbook.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <map>
#include <getopt.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>

#include <dbus-c++/dbus.h>


//// Baical

#include "P7_Trace.h"
#include "P7_Telemetry.h"


////

#include "manager.hpp"
#include "common.h"
#include "config.h"
#include "DSV.hpp"


////

using namespace std;

static struct option _long_options[] = {
     { "help",   no_argument,       0,    'h' },
     { "daemon", no_argument,       0,    'D' },
     { "config", required_argument, NULL, 'C' },
     { 0,        0,                 0,    0   }
};

static bool _daemonize()
{
     int pid = fork();
     if (pid < 0)
          exit(EXIT_FAILURE);
     if (pid > 0)
          exit(EXIT_SUCCESS);

     if (setsid() < 0) {
          cerr << "Failed to detach from previous process group" << endl;
          return false;
     }

     signal(SIGCHLD, SIG_IGN);
     signal(SIGHUP,  SIG_IGN);

     pid = fork();
     if (pid < 0)
          exit(EXIT_FAILURE);
     if (pid > 0)
          exit(EXIT_SUCCESS);

     umask(0);
     if (chdir("/") == -1) {
          cerr << "Could not change directory to '/': " << strerror(errno) << endl;
     }

     return true;
}

/**
 * Print the help message and exit.
 */
static void _print_help_and_exit(const char* pname)
{
     cout << "Usage: " << pname << " [options]" << endl
          << endl
          << "Options:" << endl
          << "  --help, -h        Print this message and exit." << endl
          << "  --daemon, -D      Run as a daemon." << endl
          << "  --config, -C      Specify the configuration file" << endl
          << endl;

     exit(0);
}

DBus::BusDispatcher dispatcher;

/**
 * @brief _sighandler -- Signal hanlder.
 */
static void _sighandler(int signum)
{
     dispatcher.leave();
     exit(0);
}


/**
 *  Entry point.
 */
int main(int argc, char** argv)
{
     int c;
     int option_index;
     char* config_file = NULL;
     bool is_daemon = false;

     while (1) {
          c = getopt_long(argc, argv,
                          "hDC:",
                          _long_options,
                          &option_index);
          if (c == -1)
               break;

          switch (c) {
          case 0:
               if (_long_options[option_index].flag != 0)
                    break;
               break;
          case 'D':
               is_daemon = true;
               break;
          case 'C':
               config_file = optarg;
               break;
          case 'h':
               _print_help_and_exit(argv[0]);
               break;
          }
     }

     if (config_file == NULL) {
          cerr << "ERROR: '--config' option is missing." << endl;
          exit(1);
     }

     if (is_daemon) {
          if (_daemonize() == false)
               throw runtime_error("Could not deamonize");
          cerr << "Daemonized." << endl;
     }

     Config config(config_file);
     string logging_configuration;
     try {
          logging_configuration = config.get("logging_configuration", false);
     } catch (out_of_range& e) {
          logging_configuration = "/P7.Sink=FileTxt /P7.Dir=/var/log/";
     }

     IP7_Client* baical_client = P7_Create_Client(TM(logging_configuration.c_str()));
     cout << "logging conf" << logging_configuration << endl;
     if (baical_client == NULL) {
          throw new runtime_error("Could not initialize logging");
     }

     baical_client->Share(P7_SHARED);
     IP7_Trace* baical_trace = P7_Create_Trace(baical_client, TM("azbook-managerd"));
     IP7_Trace::hModule baical_trace_module;
     baical_trace->Register_Thread(TM("azbook-managerd"), 0);
     baical_trace->Share(P7_SHARED_TRACE);
     baical_trace->Register_Module(TM("main"), &baical_trace_module);
     try {
          signal(SIGTERM, _sighandler);
          signal(SIGINT,  _sighandler);

          DBus::default_dispatcher = &dispatcher;
          baical_trace->P7_TRACE(baical_trace_module,
                                 TM("Connecting to DBus ..."));
          DBus::Connection conn = DBus::Connection::SystemBus();
          baical_trace->P7_TRACE(baical_trace_module,
                                 TM("Requesting DBus name ..."));
          conn.request_name(AZBOOK_MANAGER_SERVICE_NAME);
          baical_trace->P7_TRACE(baical_trace_module,
                                 TM("Starting manager ..."));
          Manager manager(conn, config);
          baical_trace->P7_TRACE(baical_trace_module,
                                 TM("Entering dispatcher ..."));
          dispatcher.enter();
     } catch (runtime_error e) {
          cerr << e.what() << endl;
          baical_client->Release();
     }
     return 0;
}

//// main.cpp ends here.

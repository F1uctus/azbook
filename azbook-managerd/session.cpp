/* session.cpp -- Azbook session implementation.
 *
 * Copyright (C) 2018-2020 "AZ Company Group" LLC <https://gkaz.ru/>
 * Copyright (C) 2018-2020 Artyom V. Poptsov <a@gkaz.ru>
 *
 * This file is part of Azbook.
 *
 * Azbook is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Azbook is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Azbook.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <unistd.h>
#include <signal.h>

#include <iostream>

#include <memory>

// getpwnam
#include <pwd.h>
#include <sys/types.h>
#include <sys/stat.h>           // chown
#include <sys/wait.h>
#include <pwd.h>
#include <unistd.h>

#include "config.h"
#include "webdav.h"
#include "config.h"
#include "session.hpp"
#include "utils.hpp"
#include "backend/webdav-backend.hpp"
#include "backend/nextcloud-backend.hpp"

using namespace std;

const string PID_DIRECTORY = "/var/run/azbook";

/**
 * @brief Session::Session -- THe main constructor of the class.
 *
 * @param config Template of a configuration file.
 * @param login User login.
 * @param password User password.
 */
Session::Session(shared_ptr<Config> config, const string& login, const string& password)
     : state(NONE),
       login(login),
       password(password),
       config(config),
       Logger_client("Session")
{
     configure_logging();
     string backend_type = config->get("backend");
     if (backend_type == "webdav") {
         backend = make_shared<WebDAV_backend>(config);
     } else if (backend_type == "nextcloud") {
         backend = make_shared<NextCloud_backend>(config);
     } else {
         LOG_CRITICAL("Could not find a backend type: %s",
                      backend_type.c_str());
         state = NONE;
     }
     LOG_TRACE("Session [%s] created", login.c_str());
}

/**
 * @brief Session::authenticate -- Try to authenticate the session user.
 */
int32_t Session::authenticate()
{
     LOG_TRACE("Performing authentication for %s ...", this->login.c_str());
     bool result = this->backend->login(this->login, this->password);
     LOG_TRACE("Authentication result: %d", result);
     if (result) {
         state = AUTHENTICATED;
         return 1;
     } else {
         LOG_CRITICAL("Authentication error in backend %s",
                      this->config->get("backend"));
         state = NONE;
         return 0;
     }
}

/**
 * @breif Session::create_user -- Create a new Azbook user if it not exists.
 *
 * @return 1 on success, 0 otherwise.
 * @throws runtime_error on errors.
 */
int32_t Session::create_user()
{
     struct stat st = { 0 };
     int result = 0;
     LOG_TRACE("Checking user %s ...", this->login.c_str());
     passwd* pw = getpwnam(this->login.c_str());
     if (pw == NULL) {
          if (! is_login_valid_p(this->login)) {
               return 0;
          }
          LOG_TRACE("Creating user %s ...", this->login.c_str());
          string command = "useradd -c '" + this->login + " (Azbook user)' -m '"
               + this->login + "'";
          if (system(command.c_str()) != 0) {
               throw runtime_error("Could not create Azbook user: " + this->login);
          }
          pw = getpwnam(this->login.c_str());
	  result = 2;
     } else {
       result = 1;
     }
     LOG_TRACE("Checking user %s ... done", this->login.c_str());

     LOG_TRACE("UID: %d", pw->pw_uid);
     if (chown(this->config->get_file_name().c_str(),
               pw->pw_uid, pw->pw_gid) != 0) {
          LOG_CRITICAL("Could not change owner of %s",
                       this->config->get_file_name().c_str());
          return 0;
     }

     return result;
}

/**
 * @brief Session::save_config -- Save the new configuration file.
 *
 * @param file_name Name of a config file.
 */
void Session::save_config(const string& file_name)
{
     LOG_TRACE("Saving configuration %s ...", file_name.c_str());
     state = CONFIG_PREPARED;
     config->set_file_name(file_name);
     config->save();
}

/**
 * @brief Session::start_daemon -- start synchronization daemon.
 *
 * @throw runtime_error
 */
void Session::start()
{
     LOG_TRACE("Starting a session for %s ...", this->login.c_str());

     if (state != CONFIG_PREPARED) {
          baical_trace->P7_WARNING(baical_trace_module,
                                   TM("Config is not prepared."));
          throw runtime_error("Config is not prepared.");
     }

     string sync_dir = config->get("synchronization_path");
     string sync_path = "/home/" + login + "/" + sync_dir;

     LOG_TRACE("Synchroniation path for '%s': '%s'",
               login.c_str(), sync_path.c_str());

     const uint32_t PATH_SZ = 256;
     char path[PATH_SZ] = { 0 };
     snprintf(path, PATH_SZ, "%s/bin/azbookd", INSTALLATION_PREFIX);

     LOG_TRACE("azbookd binary: %s", path);

     struct stat st = { 0 };
     LOG_TRACE("Checking if '%s' exists...", PID_DIRECTORY.c_str());
     if (stat(PID_DIRECTORY.c_str(), &st) == -1) {
         LOG_TRACE("Creating '%s' ...", PID_DIRECTORY.c_str());
         mkdir(PID_DIRECTORY.c_str(), 0755);
         LOG_TRACE("Creating '%s' ... done", PID_DIRECTORY.c_str());
     }

     string subdir = PID_DIRECTORY + "/" + login;
     LOG_TRACE("Checking if '%s' exists...", subdir.c_str());
     if (stat(subdir.c_str(), &st) == -1) {
         LOG_TRACE("Creating '%s' ...", subdir.c_str());
         mkdir(subdir.c_str(), 0755);
         LOG_TRACE("Creating '%s' ... done", subdir.c_str());
         LOG_TRACE("Changing owner of '%s' ...", subdir.c_str());
         passwd* pw = getpwnam(login.c_str());
         if (pw == NULL) {
             LOG_CRITICAL("Could not get passwd record for user '%d'", login.c_str());
             throw runtime_error("Could not get passwd record for " + login);
         }
         if (chown(subdir.c_str(), pw->pw_uid, pw->pw_gid)) {
             LOG_CRITICAL("Could not chown '%s' record for user '%d'",
                          subdir.c_str(),
                          login.c_str());
             throw runtime_error("Could not chown");
         }
         LOG_TRACE("Changing owner of '%s' ... done", subdir.c_str());
     }

     this->pid_file = subdir + "/azbookd.pid";

     LOG_TRACE("Forking process for %s ...", this->login.c_str());
     this->pid = fork();
     if (pid == 0) {
          struct passwd* pw = getpwnam(login.c_str());
          if (! pw) {
              cerr << "ERROR: Could not get passwd record for the user " << login << endl;
              exit(1);
          }

          if (setuid(pw->pw_uid) == -1) {
              cerr << "ERROR: Could not setuid for user " << login << endl;
              exit(1);
          }

          IP7_Client* baical_client = P7_Get_Shared(P7_SHARED);
          if (baical_client) {
               baical_client->Release();
          }

          baical_trace->Release();
          baical_trace->Unregister_Thread(0);

          execl(path, path,
                "--config", config->get_file_name().c_str(),
                "--pid-file", pid_file.c_str(),
                "--daemon",
                sync_path.c_str(),
                NULL);

          cerr << "ERROR: Could not start the azbookd for " << login << endl;
          exit(1);
     } else if (pid > 0) {
          LOG_TRACE("Forking process for %s ... done",
                    this->login.c_str());
          int status;
          waitpid(pid, &status, 0);
          LOG_TRACE("Process exit status: %d", status);
          state = DAEMON_STARTED;
     } else {
          state = DAEMON_ERROR;
          throw runtime_error("Could not start a process.");
     }
}

/**
 * Check if the session was started or not.
 */
bool Session::is_started() const
{
    return this->state == DAEMON_STARTED;
}

/**
 * Stop the session.
 */
void Session::stop()
{
     if (state != DAEMON_STARTED)
          throw runtime_error("Daemon is not running.");

     FILE* f = fopen(this->pid_file.c_str(), "r");
     if (f == NULL) {
         LOG_CRITICAL("Could not open PID file: %s", this->pid_file.c_str());
         state = NONE;
         return;
     }

     pid_t daemon_pid;
     fscanf(f, "%d", &daemon_pid);
     fclose(f);

     int status;

     LOG_TRACE("Killing the daemon process with PID %d ...", daemon_pid);
     kill(daemon_pid, SIGTERM);
     waitpid(daemon_pid, &status, 0);
     LOG_TRACE("Killing the daemon process with PID %d ... done", daemon_pid);
     LOG_TRACE("Process exit status: %d", status);

     this->backend->logout();
}

/**
 * @brief Session::cleanup -- Cleanup the session data.
 *
 * @throws runtime_error on errors.
 */
void Session::cleanup() {
    if (state != DAEMON_STARTED) {
        throw runtime_error("Daemon is not running.");
    }

    if (remove(this->config->get_file_name().c_str())) {
        LOG_CRITICAL("Could not remove the config for user '%s': %s",
                     this->login.c_str(),
                     this->config->get_file_name().c_str());
        throw runtime_error("Could not remove user config: "
                            + this->config->get_file_name());
    }
}

/**
 * @brief Session::get_pid -- Return the current PID of daemon or -1
 * if it is not running.
 */
const pid_t Session::get_pid() const
{
     return (state == DAEMON_STARTED) ? pid : -1;
}

Session_state Session::get_state() const
{
    return state;
}

//// Session.cpp ends here.

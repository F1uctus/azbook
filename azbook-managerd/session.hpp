/* session.hpp -- Azbook session header.
 *
 * Copyright (C) 2018-2020 "AZ Company Group" LLC <https://gkaz.ru/>
 * Copyright (C) 2018-2020 Artyom V. Poptsov <a@gkaz.ru>
 *
 * This file is part of Azbook.
 *
 * Azbook is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Azbook is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Azbook.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SESSION_H__
#define __SESSION_H__

#include <memory>


//// Azbook

#include "common.h"
#include "webdav.h"
#include "libazbook-common/config.h"
#include "logger-client.hpp"
#include "backend/backend.hpp"

//// Session FSM states.

enum Session_state {
     NONE,
     AUTHENTICATED,
     CONFIG_PREPARED,
     DAEMON_STARTED,
     DAEMON_ERROR
};

class Session : public Logger_client {
public:
     Session(std::shared_ptr<Config> config, const std::string& login, const std::string& password);
     int32_t authenticate();
     int32_t create_user();
     void save_config(const std::string& path);
     void start();
     void stop();
     bool is_started() const;
     const pid_t get_pid() const;
     Session_state get_state() const;

private:
     Session_state state;

     pid_t pid;

     // Credentials.
     const std::string login;
     const std::string password;
     std::string pid_file;

     std::shared_ptr<Backend> backend;

     std::shared_ptr<Config> config;

    void cleanup();
};

#endif	// ifndef __SESSION_H__

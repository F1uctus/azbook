/* manager.cpp -- Azbook manager implementation.
 *
 * Copyright (C) 2018-2020 "AZ Company Group" LLC <https://gkaz.ru/>
 * Copyright (C) 2018-2020 Artyom V. Poptsov <a@gkaz.ru>
 *
 * This file is part of Azbook.
 *
 * Azbook is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Azbook is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Azbook.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <dbus-c++/dbus.h>
#include <sys/stat.h>

#include "manager.hpp"
#include "session.hpp"
#include "common.h"

#include "webdav.h"
#include "config.h"
#include "logger-client.hpp"


//// Constants

const char* AZBOOK_MANAGER_SERVICE_NAME = "ru.gkaz.Azbook.Manager";
const char* AZBOOK_MANAGER_SERVICE_PATH = "/ru/gkaz/Azbook/Manager";

static const mode_t DEFAULT_USER_CONFIG_MODE =
     S_IRWXU | S_IRGRP | S_IROTH | S_IXOTH | S_IXGRP;
static const string CONFIG_FILE_PATH = "/etc/azbook.d/";

/**
 * Create '/etc/azbook.d/' directory.
 *
 * @throws Manager_exception
 */
static void _create_azbook_d()
{
     if ((mkdir(CONFIG_FILE_PATH.c_str(), S_IRWXU) != 0) && (errno != EEXIST)) {
          throw Manager_exception("Could not create directory: "
                                  + CONFIG_FILE_PATH);
     }
     if (chmod(CONFIG_FILE_PATH.c_str(), DEFAULT_USER_CONFIG_MODE) != 0) {
          throw Manager_exception("Could not change permissions: "
                                  + CONFIG_FILE_PATH);
     }
}

static bool _does_file_exist(string path)
{
     struct stat buffer;
     return (stat(path.c_str(), &buffer) == 0);
}


/**
 * The main constructor.
 *
 * @param connection DBus connection.
 * @param config Azbook configuration.
 * @throws runtime_error
 */
Manager::Manager(DBus::Connection& connection, const Config& config)
     : DBus::ObjectAdaptor(connection, AZBOOK_MANAGER_SERVICE_PATH),
       Logger_client("manager"),
       config(config)
{
     configure_logging();
     umask(S_IWGRP | S_IWOTH);
     if (! _does_file_exist(CONFIG_FILE_PATH)) {
          LOG_TRACE("%s", "Creating '/etc/azbook.d' ...");
          _create_azbook_d();
     }
     umask(S_IRGRP | S_IWGRP | S_IWOTH | S_IROTH);
     LOG_TRACE("%s", "Manager created");
}

/**
 * Check if a user with @p login is authenticated.
 *
 * @param string login -- A login to check.
 * @return bool if the user is authenticated, false otherwise.
 */
bool Manager::is_user_authenticated_p(const string& login)
{
     return this->sessions.find(login) != this->sessions.end();
}

int32_t Manager::Authenticate(const string& login, const string& password) {
     LOG_TRACE("Performing authentication for %s ...", login.c_str());
     Session* session = new Session(make_shared<Config>(config),
                                    login,
                                    password);
     int32_t result = session->authenticate();
     LOG_TRACE("Authentication result: %d", result);
     if (result && (! is_user_authenticated_p(login))) {
          umask(S_IWGRP | S_IWOTH);
          string config_file_path = CONFIG_FILE_PATH + login;

          this->sessions[login] = session;

          LOG_TRACE("Saving config to '%s' ...", config_file_path.c_str());
          session->save_config(config_file_path);
          LOG_TRACE("Saving config to '%s' ... done", config_file_path.c_str());
          LOG_TRACE("Creating user '%s'...", login.c_str());
          result = session->create_user();
          LOG_TRACE("Creating user '%s'... done", login.c_str());
     } else {
          if (result) {
               LOG_TRACE("User is already logged in: %s", login.c_str());
          } else {
               LOG_TRACE("Authentication failed: %s", login.c_str());
          }
          delete session;
     }
     LOG_TRACE("Result: %d", result);
     return result;
}

/**
 * Handle DBus 'StartSession' request.
 *
 * @param login User login.
 * @param password User password.
 */
int32_t Manager::StartSession(const string& login)
{
     if (! is_user_authenticated_p(login)) {
          LOG_WARNING("User %s is not authenticated", login.c_str());
          return false;
     }

     Session* session = this->sessions[login];

     try {
          if (! session->is_started()) {
               LOG_TRACE("%s", "Starting session ...");
               session->start();
               LOG_TRACE("%s", "Starting session ... done");
          }
     } catch (runtime_error* e) {
          LOG_TRACE("Authentication failed for user '%s': %s",
                    login.c_str(), e->what());
          this->sessions.erase(login);
          return 0;
     }
     LOG_TRACE("Authentication passed for user %s", login.c_str());
     return 1;
}

int32_t Manager::EndSession(const string& login)
{
    int32_t result = 1;

    if (is_user_authenticated_p(login)) {
        try {
            this->sessions[login]->stop();
        } catch (runtime_error& err) {
            LOG_CRITICAL("Could not cleanup '%s' session: %s",
                         login.c_str(),
                         err.what());
            result = 0;
        }
        this->sessions.erase(login);
    } else {
        LOG_WARNING("User '%s' is not authenticated ", login.c_str());
        result = 0;
    }

    return result;
}

vector<string> Manager::ListSessions()
{
     vector<string> result;

     for (const auto& kv : sessions) {
         if (kv.second->is_started()) {
             result.push_back(kv.first);
         }
     }

     return result;
}

static string _state_to_string(const Session_state& state)
{
     switch (state) {
     case NONE:
          return "NONE";
     case AUTHENTICATED:
          return "AUTHENTICATED";
     case CONFIG_PREPARED:
          return "CONFIG_PREPARED";
     case DAEMON_STARTED:
          return "DAEMON_STARTED";
     case DAEMON_ERROR:
         return "DAEMON_ERROR";
     default:
          return "ERROR: Wrong state";
     }
}

string Manager::SessionStatus(const string& login)
{
     Session* session = sessions[login];
     if (session == nullptr) {
          return "ERROR: No such session";
     }
     return _state_to_string(session->get_state());
}

//// Manager.cpp ends here.

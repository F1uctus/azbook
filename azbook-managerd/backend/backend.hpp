#ifndef __BACKEND__
#define __BACKEND__

#include <memory>
#include <string>
#include "logger-client.hpp"
#include "common.h"
#include "../../libazbook-common/config.h"

class Backend : public Logger_client {
public:
    Backend(std::shared_ptr<Config> config);
    virtual bool login(const std::string& login,
                       const std::string& password) = 0;
    virtual bool logout() = 0;

protected:
    std::shared_ptr<Config> config;
};

#endif  // ifndef __BACKEND__

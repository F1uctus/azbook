#include <memory>
#include "backend.hpp"
#include "config.h"

Backend::Backend(std::shared_ptr<Config> config)
    : config(config),
      Logger_client("Backend")
{
  configure_logging();
}

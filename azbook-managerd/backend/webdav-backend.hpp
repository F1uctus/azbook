#ifndef __WEBDAV_BACKEND__
#define __WEBDAV_BACKEND__

#include <memory>
#include "backend.hpp"

class WebDAV_backend : public Backend {
public:
    WebDAV_backend(std::shared_ptr<Config> config);
    bool login(const std::string& login,
               const std::string& password);
    bool logout();
};

#endif  // ifndef __WEBDAV_BACKEND__

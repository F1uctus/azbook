#ifndef __NEXTCLOUD_BACKEND_HPP__
#define __NEXTCLOUD_BACKEND_HPP__

#include <memory>
#include <string>
#include <curl/curl.h>
#include "backend.hpp"

class NextCloud_backend : public Backend {
public:
    NextCloud_backend(std::shared_ptr<Config> config);
    ~NextCloud_backend();
    bool login(const std::string& login,
               const std::string& password);
    bool logout();

private:
    void curl_configure(CURL* curl,
                        const std::string& login,
                        const std::string& password);
    std::string get_token(const std::string& buffer);
    CURL* curl;
};

#endif  // ifndef __NEXTCLOUD_BACKEND_HPP__

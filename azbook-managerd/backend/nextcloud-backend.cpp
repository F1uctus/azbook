#include <memory>
#include <string>
#include <curl/curl.h>
#include <libxml++/libxml++.h>
#include "nextcloud-backend.hpp"
#include "config.h"
#include "logger-client.hpp"
#include "backend.hpp"

using namespace std;

NextCloud_backend::NextCloud_backend(shared_ptr<Config> config)
    : Backend(config)
{
    this->curl = curl_easy_init();
    if (! this->curl) {
        LOG_CRITICAL("%s", "Could not init cURL");
        throw runtime_error("Could not init cURL");
    }
}

NextCloud_backend::~NextCloud_backend()
{
    curl_easy_cleanup(this->curl);
}

void NextCloud_backend::curl_configure(CURL* curl,
                                       const string& login,
                                       const string& password)
{
    LOG_TRACE("Configuring cURL for '%s' ...", login.c_str());
    string userpass = login + ":" + password;
    curl_easy_setopt(curl, CURLOPT_HTTPAUTH, (long) CURLAUTH_BASIC);
    curl_easy_setopt(curl, CURLOPT_USERPWD, userpass.c_str());

    struct curl_slist* headers = NULL;
    headers = curl_slist_append(headers, "OCS-APIRequest: true");
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

    curl_easy_setopt(curl, CURLOPT_STDERR, fopen("/dev/null", "w"));
    curl_easy_setopt(curl, CURLOPT_VERBOSE, 0);
    LOG_TRACE("Configuring cURL for '%s' ... done", login.c_str());
}

static int _write_callback(char* contents, size_t sz, size_t nmemb, void* userp)
{
    size_t length = sz * nmemb;
    ((string*) userp)->append(contents, length);
    return length;
}

string NextCloud_backend::get_token(const string& buffer)
{
  xmlpp::DomParser parser;
  LOG_TRACE("%s", "Parsing memory ...");
  parser.parse_memory(buffer);
  LOG_TRACE("%s", "Parsing memory ... done");
  xmlpp::Node* root_node = parser.get_document()->get_root_node();
  xmlpp::Node* data_node = root_node->get_children("data").front();
  xmlpp::Node* app_password = data_node->get_children("apppassword").front();

#if HAVE_LIBXML_CPP_2
  const auto token = dynamic_cast<const xmlpp::Element*>(app_password);
  return ((const xmlpp::ContentNode*) token->get_first_child())->get_content();
#else
  const auto token = dynamic_cast<const xmlpp::Element*>(app_password);
  return token->get_first_child_text()->get_content();
#endif
}

bool NextCloud_backend::login(const string& login,
                              const string& password)
{
    CURLcode result;
    string buffer = "";
    LOG_TRACE("Logging in '%s' ...", login.c_str());
    curl_configure(curl, login, password);
    string url = "https://"
        + config->get("webdav_server")
        + "/ocs/v2.php/core/getapppassword";
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());

    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, _write_callback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);
    LOG_TRACE("%s", "Calling cURL ...");
    result = curl_easy_perform(curl);
    LOG_TRACE("cURL result: %d", result);
    switch (result) {
    case CURLE_OK:
    {
        string token = get_token(buffer);
        LOG_TRACE("%s", "Setting configuration ...");
        this->config->put("user_name", login);
        this->config->put("user_password", token);
        LOG_TRACE("%s", "Setting configuration ... done");
        return true;
    }
    default:
        return false;
    }
}

bool NextCloud_backend::logout()
{
    CURLcode result;
    bool logout_result = true;
    CURL* curl = curl_easy_init();
    if (! curl) {
        LOG_CRITICAL("%s", "Could not init cURL");
        return false;
    }

    string login = Backend::config->get("user_name");
    string token = Backend::config->get("user_password");

    curl_configure(curl, login, token);

    string url = "https://"
        + config->get("webdav_server")
        + "/ocs/v2.php/core/apppassword";
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "DELETE");

    result = curl_easy_perform(curl);
    LOG_TRACE("cURL result: %d", result);

    if (result != CURLE_OK) {
        logout_result = false;
        LOG_CRITICAL("Could not remove a token for user '%s'",
                     login.c_str());
    }

    if (remove(Backend::config->get_file_name().c_str())) {
        LOG_CRITICAL("Could not remove the config for user '%s': %s",
                     login.c_str(),
                     Backend::config->get_file_name().c_str());
        logout_result = false;
    }

    return logout_result;
}

#ifndef __MANAGER_H__
#define __MANAGER_H__

#include <stdbool.h>

extern bool manager_start_session(pam_handle_t *pamh,
                                  const char* login);

extern bool manager_end_session(pam_handle_t *pamh,
                                const char* login);

extern int32_t manager_authenticate(pam_handle_t *pamh,
				    const char* login,
				    const char* password);

#endif	/* ifndef __MANAGER_H__ */

#include "gtest/gtest.h"
#include "../azbook-managerd/utils.hpp"

TEST(azbook_managerd_utils, is_login_valid_p)
{
    ASSERT_TRUE(is_login_valid_p("vasya"));
    ASSERT_TRUE(is_login_valid_p("alice_in_wonderland"));
    ASSERT_TRUE(is_login_valid_p("bob123"));
    ASSERT_FALSE(is_login_valid_p("1test"));
    ASSERT_FALSE(is_login_valid_p("rm -rf /"));
    ASSERT_FALSE(is_login_valid_p("vasya; su -"));
}

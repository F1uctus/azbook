#include <iostream>
#include <stdio.h>

#include "gtest/gtest.h"
#include "../libazbook-common/util.h"

TEST(utils, compare_mtimes)
{
     const string s1 = "Tue, 01 Jan 2019 02:33:23 GMT";
     const string s2 = "Tue, 01 Jan 2019 02:33:25 GMT";
     ASSERT_EQ(compare_mtimes(s1, s2), 1);
}

TEST(utils, is_current_or_parent_dir_p)
{
     ASSERT_TRUE(is_current_or_parent_dir_p("."));
     ASSERT_TRUE(is_current_or_parent_dir_p(".."));
     ASSERT_FALSE(is_current_or_parent_dir_p("directory"));
}
